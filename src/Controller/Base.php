<?php

namespace Drupal\sqrl\Controller;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\sqrl\Client;
use Drupal\sqrl\Log;
use Drupal\sqrl\Sqrl;
use Drupal\sqrl\State;
use Drupal\sqrl\StringManipulation;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Base SQRL controller with common components for all the other controllers.
 */
abstract class Base implements ContainerInjectionInterface {

  use StringManipulation;
  use StringTranslationTrait;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\sqrl\Sqrl
   */
  protected $sqrl;

  /**
   * @var \Drupal\sqrl\Client
   */
  protected $client;

  /**
   * @var \Drupal\sqrl\Log
   */
  protected $log;

  /**
   * @var \Drupal\sqrl\State
   */
  protected $state;

  /**
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Base constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   * @param \Drupal\sqrl\Sqrl $sqrl
   * @param \Drupal\sqrl\Client $client
   * @param \Drupal\sqrl\Log $log
   * @param \Drupal\sqrl\State $state
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   */
  public function __construct(RequestStack $request, EntityTypeManager $entity_type_manager, Sqrl $sqrl, Client $client, Log $log, State $state, ConfigFactory $config_factory, AccountProxyInterface $current_user) {
    $this->request = $request->getCurrentRequest();
    $this->entityTypeManager = $entity_type_manager;
    $this->sqrl = $sqrl;
    $this->client = $client;
    $this->log = $log;
    $this->state = $state;
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;

    $this->sqrl->setNut($this->client->getNut());
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('entity_type.manager'),
      $container->get('sqrl.handler'),
      $container->get('sqrl.client'),
      $container->get('sqrl.log'),
      $container->get('sqrl.state'),
      $container->get('config.factory'),
      $container->get('current_user')
    );
  }

}
