<?php

namespace Drupal\sqrl\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\sqrl\Assets;
use Drupal\sqrl\Log;
use Drupal\sqrl\Sqrl;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides the SQRL CPS controller.
 */
class CPS implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\sqrl\Assets
   */
  protected $assets;

  /**
   * @var \Drupal\sqrl\Sqrl
   */
  protected $sqrl;

  /**
   * @var \Drupal\sqrl\Log
   */
  protected $log;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var \Drupal\sqrl\Nut
   */
  private $nut;

  /**
   * Link constructor.
   *
   * @param \Drupal\sqrl\Assets $assets
   * @param \Drupal\sqrl\Sqrl $sqrl
   * @param \Drupal\sqrl\Log $log
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   */
  public function __construct(Assets $assets, Sqrl $sqrl, Log $log, AccountProxyInterface $current_user, MessengerInterface $messenger) {
    $this->assets = $assets;
    $this->sqrl = $sqrl;
    $this->log = $log;
    $this->currentUser = $current_user;
    $this->messenger = $messenger;

    $this->nut = $this->sqrl->getNewNut();
    $this->nut->fetch();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): CPS {
    return new static(
      $container->get('sqrl.assets'),
      $container->get('sqrl.handler'),
      $container->get('sqrl.log'),
      $container->get('current_user'),
      $container->get('messenger')
    );
  }

  /**
   * @param $token
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function accessLogin($token): AccessResult {
    if ($this->nut->isValid() && $this->nut->getLoginToken() === $token) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  /**
   * @param $token
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function accessCancel($token): AccessResult {
    if ($this->nut->isValid() && $this->nut->getCancelToken() === $token) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  /**
   * @param $token
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function login($token): RedirectResponse {
    if ($url = $this->nut->poll($token)) {
      return new RedirectResponse($url->toString());
    }
    return $this->cancel();
  }

  /**
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function cancel(): RedirectResponse {
    $this->messenger->addWarning($this->t('SQRL action canceled.'));
    return new RedirectResponse(Url::fromRoute('<front>')->toString());
  }

}
