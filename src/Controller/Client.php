<?php

namespace Drupal\sqrl\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\sqrl\Response\ClientResponse;

/**
 * Provides the SQRL client controller.
 */
class Client extends Base {

  /**
   * @return \Drupal\Core\Access\AccessResult
   */
  public function access(): AccessResult {
    return AccessResult::allowedIf($this->client->getNut()->isValid());
  }

  /**
   * @return \Drupal\sqrl\Response\ClientResponse
   */
  public function request(): ClientResponse {
    $content = $this->client->process();
    $this->log->debug('Client response: ' . $content);
    return new ClientResponse($content, 200, [
      'Content-Length' => strlen($content),
      'Content-Type' => 'application/x-www-form-urlencoded',
    ]);
  }

}
