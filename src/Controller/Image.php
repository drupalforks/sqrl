<?php

namespace Drupal\sqrl\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\sqrl\Response\QrCodeResponse;

/**
 * Provides the SQRL image controller.
 */
class Image extends Base {

  /**
   * @return \Drupal\Core\Access\AccessResult
   */
  public function access(): AccessResult {
    return AccessResult::allowedIf($this->client->getNut()->isValid());
  }

  /**
   * @return \Drupal\sqrl\Response\QrCodeResponse
   */
  public function request(): QrCodeResponse {
    $response = new QrCodeResponse();
    $response->setQrCodeContent($this->sqrl->getNutUrl());
    return $response;
  }

}
