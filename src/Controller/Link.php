<?php

namespace Drupal\sqrl\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\sqrl\Identities;
use Drupal\sqrl\Sqrl;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the SQRL link controller.
 */
class Link implements ContainerInjectionInterface {

  /**
   * @var \Drupal\sqrl\Sqrl
   */
  protected $sqrl;

  /**
   * @var \Drupal\sqrl\Identities
   */
  protected $identities;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Link constructor.
   *
   * @param \Drupal\sqrl\Sqrl $sqrl
   * @param \Drupal\sqrl\Identities $identities
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   */
  public function __construct(Sqrl $sqrl, Identities $identities, AccountProxyInterface $current_user, ConfigFactoryInterface $config_factory) {
    $this->sqrl = $sqrl;
    $this->identities = $identities;
    $this->currentUser = $current_user;
    $this->config = $config_factory->get('sqrl.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): Link {
    return new static(
      $container->get('sqrl.handler'),
      $container->get('sqrl.identities'),
      $container->get('current_user'),
      $container->get('config.factory')
    );
  }

  /**
   * @param \Drupal\user\UserInterface $user
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function access(UserInterface $user): AccessResult {
    if ($this->currentUser->isAuthenticated() && $this->currentUser->id() === $user->id()) {
      if (!$this->config->get('allow_multiple_ids_per_account') && !empty($this->identities->getIdentities($user->id()))) {
        return AccessResult::forbidden();
      }
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  /**
   * @return array
   */
  public function request(): array {
    return [
      'sqrl' => $this->sqrl->buildMarkup('link'),
    ];
  }

}
