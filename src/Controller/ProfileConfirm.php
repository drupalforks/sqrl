<?php

namespace Drupal\sqrl\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\user\UserDataInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides the SQRL CPS controller.
 */
class ProfileConfirm implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * Link constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   * @param \Drupal\user\UserDataInterface $user_data
   */
  public function __construct(MessengerInterface $messenger, UserDataInterface $user_data) {
    $this->messenger = $messenger;
    $this->userData = $user_data;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ProfileConfirm {
    return new static(
      $container->get('messenger'),
      $container->get('user.data')
    );
  }

  /**
   * @param \Drupal\user\UserInterface $user
   * @param string $token
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function access(UserInterface $user, $token): AccessResult {
    if ($data = $this->userData->get('sqrl', $user->id(), $token)) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  /**
   * @param \Drupal\user\UserInterface $user
   * @param string $token
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function execute(UserInterface $user, $token): RedirectResponse {
    if ($mail = $this->userData->get('sqrl', $user->id(), $token)) {
      /** @var \Drupal\user\UserInterface $account */
      $account = User::load($user->id());
      try {
        $account
          ->setEmail($mail)
          ->save();
        $this->messenger->addStatus($this->t('Your email address got confirmed successfully.'));
      }
      catch (EntityStorageException $e) {
        $this->messenger->addError($this->t('Something went wrong, please try again later.'));
      }
    }
    else {
      $this->messenger->addWarning($this->t('No data available which could be confirmed.'));
    }
    $this->userData->delete('sqrl', $user->id(), $token);
    return new RedirectResponse(Url::fromRoute('user.page')->toString());
  }

}
