<?php

namespace Drupal\sqrl\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\sqrl\Assets;
use Drupal\sqrl\Sqrl;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the SQRL link controller.
 */
class View implements ContainerInjectionInterface {

  /**
   * @var \Drupal\sqrl\Assets
   */
  protected $assets;

  /**
   * @var \Drupal\sqrl\Sqrl
   */
  protected $sqrl;

  /**
   * Link constructor.
   *
   * @param \Drupal\sqrl\Assets $assets
   * @param \Drupal\sqrl\Sqrl $sqrl
   */
  public function __construct(Assets $assets, Sqrl $sqrl) {
    $this->assets = $assets;
    $this->sqrl = $sqrl;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): View {
    return new static(
      $container->get('sqrl.assets'),
      $container->get('sqrl.handler')
    );
  }

  /**
   * @param string $op
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function access($op = NULL): AccessResult {
    // TODO: check context like anonymous user etc.
    return AccessResult::allowedIf($op !== NULL && in_array($op, ['register', 'login', 'link', 'unlink', 'profile']));
  }

  /**
   * @param $op
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function getTitle($op): TranslatableMarkup {
    return $this->assets->getOperationTitle($op);
  }

  /**
   * @param string $op
   *
   * @return array
   */
  public function request($op): array {
    return [
      'sqrl' => $this->sqrl->buildMarkup($op),
    ];
  }

}
