<?php

namespace Drupal\sqrl;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * FormAlter service.
 */
class FormAlter {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * @var \Drupal\sqrl\Sqrl
   */
  protected $sqrl;

  /**
   * @var \Drupal\sqrl\Log
   */
  protected $log;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\sqrl\Identities
   */
  protected $identities;

  /**
   * Constructs a FormAlter object.
   *
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current user.
   * @param \Drupal\sqrl\Sqrl $sqrl
   *   The SQRL handler.
   * @param \Drupal\sqrl\Log $log
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\sqrl\Identities $identities
   */
  public function __construct(AccountProxy $current_user, Sqrl $sqrl, Log $log, EntityTypeManagerInterface $entity_type_manager, Identities $identities) {
    $this->currentUser = $current_user;
    $this->sqrl = $sqrl;
    $this->log = $log;
    $this->entityTypeManager = $entity_type_manager;
    $this->identities = $identities;
  }

  /**
   * @param array $form
   */
  public function loginForm(array &$form): void {
    $this->addMarkup($form, 'login');
    $form['#validate'][] = [$this, 'loginFormValidate'];
  }

  /**
   * @param array $form
   */
  public function registerForm(array &$form): void {
    if ($this->currentUser->isAnonymous()) {
      $this->addMarkup($form, 'register');
    }
  }

  /**
   * @param array $form
   */
  public function passwordForm(array &$form): void {
    $form['#validate'][] = [$this, 'passwordFormValidate'];
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function userForm(array &$form, FormStateInterface $form_state): void {
    if (!isset($form_state->getStorage()['user'])) {
      // This is the registration form.
      return;
    }
    /** @var \Drupal\user\UserInterface $user */
    $user = $form_state->getStorage()['user'];
    if ($user->id() !== $this->currentUser->id() || !$this->identities->hasUserEnabledIdentities($user->id())) {
      return;
    }
    $form['account']['mail']['#access'] = FALSE;
    $form['account']['pass']['#access'] = FALSE;
    $form['account']['current_pass']['#access'] = FALSE;
    $form['sqrl'] = $this->sqrl->buildMarkup('profile');
    $form['sqrl']['#weight'] = -99;
  }

  /**
   * @param array $form
   * @param string $op
   */
  private function addMarkup(array &$form, $op): void {
    $form['sqrl'] = $this->sqrl->buildMarkup($op);
    $this->alterForm($form);
  }

  /**
   * @param array $form
   */
  private function alterForm(array &$form): void {
    // Nothing to do at this point.
  }

  private function findAccountFromFormInput(FormStateInterface $form_state) {
    try {
      $user_storage = $this->entityTypeManager->getStorage('user');
      $name = trim($form_state->getValue('name'));
      $users = $user_storage->loadByProperties(['mail' => $name]);
      if (empty($users)) {
        $users = $user_storage->loadByProperties(['name' => $name]);
      }
      $account = reset($users);
      if ($account && $account->id()) {
        return $account;
      }
    }
    catch (InvalidPluginDefinitionException $e) {
    }
    catch (PluginNotFoundException $e) {
    }
    return NULL;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function loginFormValidate(array $form, FormStateInterface $form_state): void {
    if ($account = $this->findAccountFromFormInput($form_state)) {
      /** @var \Drupal\sqrl\Entity\IdentityInterface $identity */
      foreach ($this->identities->getIdentities($account->id()) as $identity) {
        if ($identity->isSqrlOnly()) {
          $form_state->setErrorByName('name', $this->t('%name can only login with SQRL.', ['%name' => $account->label()]));
          return;
        }
      }
    }
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function passwordFormValidate(array $form, FormStateInterface $form_state): void {
    if ($account = $this->findAccountFromFormInput($form_state)) {
      /** @var \Drupal\sqrl\Entity\IdentityInterface $identity */
      foreach ($this->identities->getIdentities($account->id()) as $identity) {
        if ($identity->isHardLocked()) {
          $form_state->setErrorByName('name', $this->t('%name is hard locked by SQRL client request.', ['%name' => $account->label()]));
          return;
        }
      }
    }
  }

}
