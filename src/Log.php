<?php

namespace Drupal\sqrl;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Log service.
 */
class Log {

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a FormAlter object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger) {
    $this->config = $config_factory->get('sqrl.settings');
    $this->logger = $logger->get('sqrl');
  }

  /**
   * @param string $msg
   * @param array $parameters
   */
  public function debug($msg, $parameters = []): void {
    if ($this->config->get('debug')) {
      $this->logger->debug($msg, $parameters);
    }
  }

  /**
   * @param string $msg
   * @param array $parameters
   */
  public function error($msg, $parameters = []): void {
    $this->logger->error($msg, $parameters);
  }

}
