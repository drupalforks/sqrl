<?php

namespace Drupal\sqrl\Response;

use Symfony\Component\HttpFoundation\Response;

/**
 * Response which return plain text content to the SQRL client.
 */
class ClientResponse extends Response {

  /**
   * {@inheritdoc}
   */
  public function sendHeaders(): Response {
    $this->headers->set('Content-Type', 'application/x-www-form-urlencoded');
    $this->headers->set('Content-Length', strlen($this->content));
    return parent::sendHeaders();
  }

}
