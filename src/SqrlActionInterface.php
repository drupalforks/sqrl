<?php

namespace Drupal\sqrl;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Interface for sqrl_action plugins.
 */
interface SqrlActionInterface extends ContainerFactoryPluginInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label(): string;

  /**
   * @return bool
   */
  public function requiresSignatureRevalidation(): bool;

  /**
   * @return bool
   * @throws \Drupal\sqrl\Exception\ClientException
   */
  public function run(): bool;

  /**
   * @return $this
   */
  public function rememberSuccess(): SqrlActionInterface;

}
