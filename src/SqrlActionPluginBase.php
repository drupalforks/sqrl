<?php

namespace Drupal\sqrl;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\Random;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for sqrl_action plugins.
 */
abstract class SqrlActionPluginBase extends PluginBase implements SqrlActionInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\sqrl\Sqrl
   */
  protected $sqrl;

  /**
   * @var \Drupal\sqrl\Client
   */
  protected $client;

  /**
   * @var \Drupal\sqrl\State
   */
  protected $state;

  /**
   * @var \Drupal\sqrl\Log
   */
  protected $log;

  /**
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * @var \Drupal\Component\Utility\Random
   */
  protected $random;

  /**
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * @var \Drupal\sqrl\Identities
   */
  protected $identities;

  /**
   * @var \Drupal\user\UserInterface
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Sqrl $sqrl, Client $client, State $state, Log $log, ConfigFactory $config_factory, TimeInterface $time, Identities $identities) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->sqrl = $sqrl;
    $this->client = $client;
    $this->state = $state;
    $this->log = $log;
    $this->configFactory = $config_factory;
    $this->time = $time;
    $this->identities = $identities;

    $this->random = new Random();
    $this->account = $this->client->getAccount();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('sqrl.handler'),
      $container->get('sqrl.client'),
      $container->get('sqrl.state'),
      $container->get('sqrl.log'),
      $container->get('config.factory'),
      $container->get('datetime.time'),
      $container->get('sqrl.identities')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function requiresSignatureRevalidation(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function rememberSuccess(): SqrlActionInterface {
    $public_nut = $this->client->getNut()->getPublicNut();
    if ($this->account) {
      $this->state->setAuth($public_nut, $this->account->id());
    }
    elseif ($this->client->getIdentity()) {
      $this->state->setAuth($public_nut, $this->client->getIdentity()->getUsers());
    }
    return $this;
  }

}
