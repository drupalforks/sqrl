<?php

namespace Drupal\sqrl;

use Drupal\Core\KeyValueStore\KeyValueExpirableFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * State service.
 */
final class State extends KeyValueExpirableFactory {

  public const EXPIRE_AUTH = 120;
  public const EXPIRE_NUT = 600;

  /**
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected $auth;

  /**
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected $nut;

  /**
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected $messages;

  /**
   * {@inheritdoc}
   */
  public function __construct(ContainerInterface $container, array $options = []) {
    parent::__construct($container, $options);
    $this->auth = $this->get('sqrl_auth');
    $this->nut = $this->get('sqrl_nut');
    $this->messages = $this->get('sqrl_messages');
  }

  /**
   * @param string $nut
   * @param \Drupal\user\UserInterface[]|int $users
   *
   * @return \Drupal\sqrl\State
   */
  public function setAuth($nut, $users): State {
    if (is_array($users)) {
      $uids = [];
      foreach ($users as $user) {
        $uids[] = $user->id();
      }
    }
    else {
      $uids = [$users];
    }
    $this->auth->setWithExpire($nut, json_encode($uids), self::EXPIRE_AUTH);
    return $this;
  }

  /**
   * @param string $nut
   * @param array $values
   *
   * @return \Drupal\sqrl\State
   */
  public function setNut($nut, array $values): State {
    $this->nut->setWithExpire($nut, json_encode($values), self::EXPIRE_NUT);
    return $this;
  }

  /**
   * @param string $nut
   * @param bool $delete
   *
   * @return int[]
   */
  public function getAuth($nut, $delete = TRUE): array {
    if ($data = $this->auth->get($nut)) {
      if ($delete) {
        $this->auth->delete($nut);
      }
      return json_decode($data, TRUE);
    }
    return [];
  }

  /**
   * @param string $nut
   *
   * @return array
   */
  public function getNut($nut): array {
    if ($data = $this->nut->get($nut)) {
      return json_decode($data, TRUE);
    }
    return [];
  }

  /**
   * @param string $nut
   * @param string|\Drupal\Component\Render\MarkupInterface $message
   * @param string $type
   *
   * @return \Drupal\sqrl\State
   */
  public function addMessage($nut, $message, $type = MessengerInterface::TYPE_STATUS): State {
    $messages = $this->getMessages($nut);
    $messages[] = [
      'message' => $message,
      'type' => $type,
    ];
    $this->messages->setWithExpire($nut, json_encode($messages), self::EXPIRE_NUT);
    return $this;
  }

  /**
   * @param string $nut
   *
   * @return array
   */
  public function getMessages($nut): array {
    if ($data = $this->messages->get($nut)) {
      return json_decode($data, TRUE);
    }
    return [];
  }

}
