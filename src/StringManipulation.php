<?php

namespace Drupal\sqrl;

use Exception;

trait StringManipulation {

  /**
   * @param int $length
   *   Length of the password to be generated.
   *
   * @return string
   */
  protected function randomBytes($length): string {
    $randomBytes = '';
    try {
      $randomBytes = random_bytes($length);
    }
    catch (Exception $e) {
      // TODO: Error handling.
    }
    return $randomBytes;
  }

  /**
   * Returns a URL safe base64 encoded version of the string.
   *
   * @param string $string
   * @return string
   */
  protected function base64_encode($string): string {
    $data = base64_encode($string);
    // Modify the output so it's safe to use in URLs.
    return strtr($data, ['+' => '-', '/' => '_', '=' => '']);
  }

  /**
   * Returns the base64 decoded version of the URL safe string.
   *
   * @param string $string
   * @return string
   */
  protected function base64_decode($string): string {
    $data = strtr($string, ['-' => '+', '_' => '/']);
    return base64_decode($data);
  }

}
